#!/bin/bash

INVENTORY=~/.rapid_ansible_hosts

echo localhost ansible_connection=local > $INVENTORY

ADMIN='yes'
XUID=''
while getopts "a:u:" flag; do
	case "$flag" in
		a) ADMIN=$OPTARG;;
		u) XUID=$OPTARG;;
	esac
done
XUSERNAME=${@:$OPTIND:1}


if [[ "${XUSERNAME}" == "" ]]; then
	echo "Usage: $0 [options] USERNAME"
	exit 2
fi

if [ $# == 1 ]; then
	XUSERNAME=$1
	XUID=''
elif [ $# == 2 ]; then
	XUSERNAME=$1
	XUID=$2
fi

## In newer versions of ansible, the ssh key module was moved
## Check installed ansible the newer rev (double digit) and install
## now optional module
if ansible --version | grep "^ansible" |  grep -q "2.[0-9][0-9]"
then
  ## There is a bug in the ansible-core + ubuntu22.04 combination that prevents
  ## ansible-galaxy from running correctly. To work around this bug, resolvelib
  ## must be downgraded
  ## https://bugs.launchpad.net/ubuntu/+source/ansible/+bug/1995249

  ## Using set -e here so that the script will exit if any commands before the #
  ## +e fail. This is to prevent the script from continuing to run if one of the #
  ## pre-reqs fails. We may want to look at expanding this to the entire script,
  ## but I'm unsure of the implications of that right now - DS
  set -e
  if lsb_release -r | grep 22.04 &>/dev/null
  then

    dpkg -l python3-pip || \
        DEBIAN_FRONTEND=noninteractive apt-get -y update && \
        DEBIAN_FRONTEND=noninteractive apt-get -y install python3-pip
    pip install resolvelib==0.5.4
  fi
  ansible-galaxy collection install ansible.posix
  set +e
fi

## Do the install here
ansible-playbook  -i $INVENTORY ./main.yaml --extra-vars="username=${XUSERNAME} uid=${XUID} admin=${ADMIN}" | tee /tmp/rapid_image_status.txt

if [[ ${PIPESTATUS[0]} == 0 ]]; then
	echo "Ansible Playbook Succeeded"
	echo "success" > /tmp/rapid_image_complete
    exit 0
else
	echo "Ansible playbook failed"
	echo "fail" > /tmp/rapid_image_complete
    exit 1
fi

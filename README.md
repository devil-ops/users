## Synopsis

This script will set up kerberos authentication, users and sudo rules

## Requirements

Ansible

Sudo rights on the server

RHEL or Ubuntu

## Usage

If you know the UID, go ahead and pass it in

```
$ sudo ./install.sh [-a yes|no] [-u UID] USERNAME
```
Right now it defaults to admin = yes.  To add a non admin user, use '-a no'

If you don't know the uid, we will try and pull it from LDAP

```
$ sudo ./install.sh USERNAME
```

